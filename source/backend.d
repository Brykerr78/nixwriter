module backend;
debug
{
    public import std.stdio : stdout, write, writef, writeln, writefln;
}

import std.process : executeShell;
import std.typecons : tuple, Tuple;

private immutable string lsblkCommand = "lsblk --paths --nodeps --include 8 --list --output NAME,TRAN,VENDOR,MODEL,SIZE,RM --noheadings --sort SIZE";
/*
    lsblk --paths --nodeps --include 8 --list --output NAME,TRAN,VENDOR,MODEL,SIZE,RM --noheadings --sort SIZE
    /dev/sdc usb    Lexar    USB_Flash_Drive          7.5G  1
    /dev/sdb sata   ATA      KINGSTON_SUV400S37120G 111.8G  0
    /dev/sda sata   ATA      ST1000DM010-2EP102     931.5G  0
    /dev/sdd usb    WD       WDC_WD20EZRZ-00Z5HB0     1.8T  0
*/
public string[] getDeviceList()
{
    auto text = executeShell(lsblkCommand).output;
    import std.algorithm : endsWith;
    import std.algorithm.iteration : filter, map;
    import std.string : lineSplitter, stripRight;
    import std.range : array, dropBackOne;

    return text.lineSplitter().filter!(line => line.endsWith('1'))
        .map!(line => line.dropBackOne().stripRight())
        .array();
}

public bool deviceExists(in string deviceName)
{
    import std.algorithm.searching : any;
    import std.range : front, split;

    return getDeviceList().any!(line => line == deviceName);
}

public string parseDeviceName(in string deviceSummary)
{
    import std.range : split, front;
    import std.algorithm.searching : startsWith;

    return deviceSummary.split().front();
}

public class Calldd
{
private:
    const string tmpFileName = "/tmp/nixwriter.progress.txt";
    string deviceName, sourceFileName;

public:
    this(in string sourceFileName, in string deviceName)
    {
        this.sourceFileName = sourceFileName;
        this.deviceName = deviceName;
    }

    int dd()
    {
        static ddcount = 0;
        ++ddcount;
        scope (success)
            --ddcount;
        if (ddcount > 1)
            return 1;
        // Running dd again should be made impossible by the user from the UI level
        // I'm not making the code any more complicated

        import std.format : format;

        immutable string command = format("pkexec dd if=/%s of=%s bs=4M status=progress 2>%s && sync",
                sourceFileName, deviceName, tmpFileName);

        return executeShell(command).status;
    }

    double parse()
    {
        import std.file : readText, getSize, exists;
        import std.string : splitLines, isNumeric;
        import std.range : front, back, split;
        import std.conv : to;
        import std.algorithm.searching : canFind, count;

        try
        {
            if (!(exists(tmpFileName)))
            {
                return 0;
            }

            auto text = tmpFileName.readText();
            if (text.length == 0)
            {

                return 0;
            }
            else if ((text.count('\n') == 0) && (text.splitLines().back()
                    .split().front().isNumeric()))
            {

                return text.splitLines().back().split().front()
                    .to!ulong / (getSize(sourceFileName).to!double);
            }

            else
            {
                return 0;
            }
        }
        catch (Exception e)
        {
            // Sanitize exceptions
        }
        return 0;
    }

    ~this()
    {
        import std.file : exists, remove;

        if (tmpFileName.exists())
            remove(tmpFileName);
    }

}

public void trynotify()
{
    import std.process : executeShell;

    executeShell(`notify-send 'Nixwriter' 'Operation completed' --icon=dialog-information`);
}

// tool for expanding pretty file names into actual byes
// assert(7.5G == 7.5 * 1024^9);
private ulong inBytes(in string sz)
{
    import std.conv : to;
    import std.math : pow;

    ulong factor = 1;
    ulong quotient = 1024;
    switch (sz[$ - 1])
    {
    case 'K':
        factor *= pow(quotient, 1);
        break;
    case 'M':
        factor *= pow(quotient, 2);
        break;
    case 'G':
        factor *= pow(quotient, 3);
        break;
    case 'T':
        factor *= pow(quotient, 4);
        break;
    case 'P':
        factor *= pow(quotient, 5); //TODO: fix possible overflows
        break;
    case 'E':
        factor *= pow(quotient, 6);
        break;
    case 'Z':
        factor *= pow(quotient, 7);
        break;
    case 'Y':
        factor *= pow(quotient, 8);
        break;
    default:
        factor = 1;
    }
    return cast(ulong) sz[0 .. $ - 1].to!double * factor;
}

public bool pathCheck()
{
    import std.file : exists;

    const searchPaths = ["/usr/bin/", "/bin/"];
    const searchBins = ["dd", "pkexec", "lsblk"];
    foreach (bin; searchBins)
    {
        bool found;
        foreach (path; searchPaths)
        {

            if (exists(path ~ bin))
            {
                found = true;
            }
        }
        if (!found)
        {
            return false;
        }
    }
    return true;
}

public Tuple!(bool, "pass", string, "reason") verboseSanityCheck(
        in string sourceFileName, in string comboTextInput)
{
    Tuple!(bool, "pass", string, "reason") result;
    result.pass = false;
    result.reason = "unknown";

    import std.file : exists, getSize;
    import std.range : split, back;

    if (!(exists(sourceFileName)))
        result.reason = "File not found";
    else if (!(deviceExists(comboTextInput)))
        result.reason = "Device not found";
    else if (getSize(sourceFileName) >= inBytes(comboTextInput.split().back()))
        result.reason = "File too big for the device";
    else
    {
        result.pass = true;
        result.reason = "All checks pass";
        return result;
    }

    return result;
}
