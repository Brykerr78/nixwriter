import app;

int main(string[] args)
{
    auto app = new App();
    return app.run(args);
}
