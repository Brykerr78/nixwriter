module ui;
import backend;

import gtk.Application : Application;
import gtk.ApplicationWindow : ApplicationWindow;
import gtk.HeaderBar : HeaderBar;
import gtk.Box : Box;
import gtkc.gtktypes : GtkOrientation, GtkRevealerTransitionType, FileChooserAction;
import gtk.Revealer : Revealer;
import gtk.FileChooserButton : FileChooserButton;
import gtk.Label : Label;
import gtk.ProgressBar : ProgressBar;
import gtk.Widget : Widget;
import gtk.FileFilter : FileFilter;
import gtk.Button : Button;
import gtk.ComboBoxText : ComboBoxText;

public class PrimaryWindow : ApplicationWindow
{
private:
    static const string title = "Nixwriter";
    static const GtkRevealerTransitionType revealerType = GtkRevealerTransitionType.SLIDE_DOWN;
    static const uint revealerDurationMsecs = 144;
    enum PADDING
    {
        BORDER = 10,
        HORIZONTAL = 16,
        VERTICAL = 10
    }

    enum DEFAULT_DIMENSION
    {
        HEIGHT = 480,
        WIDTH = 200
    }

    HeaderBar hb;
    Box mainVbox, sourceBox, deviceBox, triggerBox, progressBox;
    Revealer deviceBoxRevealer, triggerBoxRevealer, progressBoxRevealer;
    Label sourceBoxLabel, deviceBoxLabel;
    FileChooserButton fcb;
    Button reload, trigger;
    ComboBoxText deviceCombo;
    ProgressBar progress;

    static HeaderBar genHeader(const string title)
    {
        auto hb = new HeaderBar();
        hb.setTitle(title);
        hb.setShowCloseButton(true);
        return hb;
    }

    static Revealer genRevealer(ref Box box, Widget[] widgets...)
    {
        auto rev = new Revealer();
        rev.setTransitionType(revealerType);
        rev.setTransitionDuration(revealerDurationMsecs);

        foreach (i, widget; widgets)
        {
            if (i == 0)
                if (widgets.length == 1)
                    box.packStart(widget, true, false, 0); // true => center
                else
                    box.packStart(widget, false, false, 0);
            else
                        box.packEnd(widget, false, false, 0); // lol @ dfmt
        }
        rev.add(box);
        return rev;
    }

    static Revealer genRevealer(ref Box box, bool expand, bool fill, Widget[] widgets...)
    {
        auto rev = new Revealer();
        rev.setTransitionType(revealerType);
        rev.setTransitionDuration(revealerDurationMsecs);

        foreach (i, widget; widgets)
        {
            if (i == 0)
                if (widgets.length == 1)
                    box.packStart(widget, expand, fill, 0); // true => center
                else
                    box.packStart(widget, expand, fill, 0);
            else
                        box.packEnd(widget, expand, fill, 0); // lol @ dfmt
        }
        rev.add(box);
        return rev;
    }

    static Box genHbox()
    {
        return new Box(GtkOrientation.HORIZONTAL, PADDING.HORIZONTAL);
    }

    static FileChooserButton genFcb()
    {
        import std.process : environment;

        auto fcb = new FileChooserButton("Select your bootable image file", FileChooserAction.OPEN);
        fcb.setCurrentFolder(environment["HOME"]);
        auto filter = new FileFilter();
        filter.addPattern("*.iso");
        filter.addPattern("*.img");
        filter.setName(".iso/.img");
        fcb.addFilter(filter);
        return fcb;
    }

    static Button genReloader()
    {
        import gtkc.gtktypes : GtkIconSize;

        auto btn = new Button("view-refresh", GtkIconSize.BUTTON);
        btn.setTooltipText("Refresh device list");
        return btn;
    }

    static ComboBoxText genCombo()
    {
        auto combo = new ComboBoxText(false);
        auto devices = getDeviceList();
        if (devices.length == 0)
            combo.appendText("");
        else
            foreach (device; devices)
                combo.appendText(device);
        return combo;
    }

    static ProgressBar genProgress()
    {
        auto pb = new ProgressBar();
        pb.setFraction(0.0);
        pb.setText("Progress");
        return pb;
    }

    void deactivateInputs()
    {
        fcb.setSensitive(false);
        deviceCombo.setSensitive(false);
        trigger.setSensitive(false);
        reload.setSensitive(false);

        fcb.setTooltipText("Program is busy, can't select files.");
        deviceCombo.setTooltipText("Program is busy, can't select device.");
        trigger.setTooltipText("Program busy, can't write.");
        reload.setTooltipText("Program busy, please be patient");
    }

    void activateInputs()
    {
        fcb.setSensitive(true);
        deviceCombo.setSensitive(true);
        trigger.setSensitive(true);
        reload.setSensitive(false);
        fcb.setTooltipText("Choose another file.");
        deviceCombo.setTooltipText("Select a device.");
        trigger.setTooltipText("Write media");
        reload.setTooltipText("Refresh device list");
    }

    void updateTrigger()
    {
        auto sanity = verboseSanityCheck(fcb.getFilename, deviceCombo.getActiveText);
        trigger.setSensitive(triggerBoxRevealer.getRevealChild() && sanity.pass);
        trigger.setTooltipText(trigger.getSensitive() ? "Write media" : sanity.reason);
    }

public:
    this(Application app)
    {
        super(app);
        setBorderWidth(PADDING.BORDER);
        setDefaultSize(DEFAULT_DIMENSION.WIDTH, 0);
        hb = genHeader(title);
        setTitlebar(hb);

        mainVbox = new Box(GtkOrientation.VERTICAL, PADDING.VERTICAL);
        scope (success)
            add(mainVbox);

        sourceBox = genHbox();
        sourceBoxLabel = new Label("Source file");
        fcb = genFcb();
        sourceBox.packStart(sourceBoxLabel, false, false, 0);
        sourceBox.packEnd(fcb, false, false, 0);
        mainVbox.packStart(sourceBox, false, false, 0);

        deviceBox = genHbox();
        deviceBoxLabel = new Label("Flash device");
        reload = genReloader();
        deviceCombo = genCombo();
        deviceBox = genHbox();
        deviceBox.packStart(deviceBoxLabel, false, false, 0);
        deviceBox.packEnd(reload, false, false, 0);
        deviceBox.packEnd(deviceCombo, false, false, 0);
        // deviceBoxRevealer = genRevealer(deviceBox, deviceBoxLabel, reload, deviceCombo);
        deviceBoxRevealer = new Revealer();
        deviceBoxRevealer.setTransitionType(GtkRevealerTransitionType.SLIDE_DOWN);
        deviceBoxRevealer.setTransitionDuration(revealerDurationMsecs);
        deviceBoxRevealer.add(deviceBox);
        mainVbox.packStart(deviceBoxRevealer, false, false, 0);

        reload.addOnClicked(delegate void(Button _) {
            deviceCombo.removeAll();
            auto devices = getDeviceList();
            if (devices.length == 0)
                deviceCombo.appendText("");
            else
                foreach (device; devices)
                    deviceCombo.appendText(device);
        });

        fcb.addOnFileSet(delegate void(FileChooserButton _) {
            deviceBoxRevealer.setRevealChild(true);
        });

        triggerBox = genHbox();
        trigger = new Button("Write");
        triggerBoxRevealer = genRevealer(triggerBox, trigger);
        mainVbox.packEnd(triggerBoxRevealer, false, false, 0);

        deviceCombo.addOnChanged(delegate void(ComboBoxText _) {
            triggerBoxRevealer.setRevealChild(deviceBoxRevealer.getRevealChild());
            updateTrigger();
        });

        progress = new ProgressBar();
        progressBox = genHbox();
        progressBoxRevealer = genRevealer(progressBox, true, true, progress);
        mainVbox.packStart(progressBoxRevealer, false, false, 0);

        trigger.addOnClicked(delegate void(Button _) {
            updateTrigger();
            if (trigger.getSensitive())
            {
                trigger.setSensitive(true);
                deactivateInputs();

                progressBoxRevealer.setRevealChild(true);

                auto sourceFileName = fcb.getFilename();
                auto deviceName = parseDeviceName(deviceCombo.getActiveText());

                auto ddCall = new Calldd(sourceFileName, deviceName);
                import std.parallelism : task;

                auto proc = task(&ddCall.dd);
                proc.executeInNewThread();

                import glib.Timeout : Timeout;
                import gtkc.gtktypes : GPriority;

                auto timeout = new Timeout(500, delegate bool() {
                    auto percentage = ddCall.parse();

                    if (proc.done)
                    {
                        deviceBoxRevealer.setRevealChild(false);
                        triggerBoxRevealer.setRevealChild(false);
                        progressBoxRevealer.setRevealChild(false);
                        deviceCombo.setActiveText("");
                        hb.setSubtitle("");
                        // notify system if possible
                        trynotify();
                        activateInputs();
                        return 0;
                    }
                    if (percentage == 0.0)
                    {
                        progress.pulse();
                        hb.setSubtitle("Preparing device, please be patient.");
                    }
                    else
                    {
                        if (percentage < 99.9) // avoid the awkward scientific format 
                        {
                            progress.setFraction(percentage);
                            import std.format : format;

                            hb.setSubtitle(format("Writing media: %2.2f", percentage * 100) ~ `%`);
                        }
                    }
                    return true;

                }, GPriority.HIGH);
            }
        });
    }
}

public class MessageWinow : ApplicationWindow
{
public:
    this(Application app)
    {
        super(app);
        setBorderWidth(20);
        setTitle("Nixwriter: Error");
        setDefaultSize(320, 200);
        add(new Label("Can't find dd, pkexec or lsblk in /usr/bin or /bin"));
    }
}
