module app;
import ui;
import backend : pathCheck;
import gtk.Application : Application;
import gio.Application : GioApp = Application;
import gtkc.gtktypes : GApplicationFlags;

public class App : Application
{
public:
    this(const string appID = "org.nixwriter.ui",
            const GApplicationFlags flags = GApplicationFlags.FLAGS_NONE)
    {
        super(appID, flags);

        addOnActivate(delegate void(GioApp _) {
            if (!(pathCheck()))
            {
                auto mw = new MessageWinow(this);
                mw.showAll();
            }
            else
            {
                auto pw = new PrimaryWindow(this);
                pw.showAll();
            }
        });
    }
}
