# Nixwriter - Create Bootable Linux images

## Dependencies 

* A D compiler and `libphobos`
* Dlang package manager, dub 
* GTK+3 devel libraries
* A failry recent version of GNU [lsblk](http://man7.org/linux/man-pages/man8/lsblk.8.html) and [dd](http://man7.org/linux/man-pages/man1/dd.1.html)
* [pkexec](https://www.freedesktop.org/software/polkit/docs/0.105/pkexec.1.html)
* [libnotify](https://github.com/GNOME/libnotify) (optional)

## Sample screenshots

![](https://i.imgur.com/srz9H4K.png)

![](https://i.imgur.com/Q3tdJg6.png)

![](https://i.imgur.com/tYi1Wam.png)

![](https://i.imgur.com/ZZILrda.png)

![](https://i.imgur.com/iXtH0XJ.png)

![](https://i.imgur.com/ISMS7I4.png)

## Installation

* `git clone` this repo and `cd` into it
* Issue `dub build -b release -defaultlib`. If you have `ldc` or `gdc` installed (which is recommended), you should append `--compiler ldc` or `--compiler gdc` for better performance, and copy the binary from the `bin/` directory to `/usr/bin` with proper privilege.